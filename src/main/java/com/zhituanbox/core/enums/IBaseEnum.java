package com.zhituanbox.core.enums;

import cn.hutool.core.util.ObjectUtil;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 枚举基类
 * <p>实现的枚举都继承这个接口</p>
 * @author LiuYang
 * @since 2020/9/25
 */
public interface IBaseEnum<T> extends Serializable {

    /**
     * 枚举数据库存储值
     * @return T
     */
    T getValue();

    /**
     * 说明
     * @return desc
     */
    String getDesc();

    /**
     * 是否相等
     * @param value 比较值
     * @return true：相等
     * @see ObjectUtil#equal(Object, Object)
     */
    default boolean equalsValue(T value) {
        return ObjectUtil.equal(getValue(), value);
    }

    /**
     * 将枚举的值与Value描述转换成单个对象
     * @return 转换成单个对象，方便跨系统传输
     */
    @NotNull
    default SingleKeyValue<T> toKeyValue() {
        return new SingleKeyValue<>(this.getValue(), this.getDesc());
    }
}
