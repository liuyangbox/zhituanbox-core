package com.zhituanbox.core.enums;

/**
 * 单个键值对
 * <p>用于枚举输出值与描述</p>
 *
 * @author LiuYang
 * @since 2020/7/23
 * @version 1.0.0
 */
public class SingleKeyValue<T> {
    /**
     * 值
     */
    private T value;
    /**
     * 描述
     */
    private String desc;

    /**
     * <p>Constructor for SingleKeyValue.</p>
     */
    public SingleKeyValue() {
    }

    /**
     * <p>Constructor for SingleKeyValue.</p>
     *
     * @param value a T object.
     * @param desc a {@link java.lang.String} object.
     */
    public SingleKeyValue(T value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a T object.
     */
    public T getValue() {
        return value;
    }

    /**
     * <p>Setter for the field <code>value</code>.</p>
     *
     * @param value a T object.
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * <p>Getter for the field <code>desc</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDesc() {
        return desc;
    }

    /**
     * <p>Setter for the field <code>desc</code>.</p>
     *
     * @param desc a {@link java.lang.String} object.
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
