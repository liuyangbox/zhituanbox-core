package com.zhituanbox.core.exception;

import cn.hutool.core.util.StrUtil;

import javax.validation.constraints.NotNull;

/**
 * 业务异常
 *
 * @author LiuYang
 * @since 2020/5/28
 * @version 1.0.0
 */
public class BusinessException extends RuntimeException {
    /**
     * 异常编码
     */
    private final String code;
    /**
     * 编码对应参数
     */
    private Object[] codeArgs;

    /**
     * <p>Constructor for BusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param cause a {@link java.lang.Throwable} object.
     * @param code a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     */
    public BusinessException(@NotNull String message, Throwable cause, String code, Object... params) {
        super(StrUtil.format(message, params), cause);
        this.code = code;
    }

    /**
     * <p>Constructor for BusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     */
    public BusinessException(@NotNull String message, String code, Object... params) {
        super(StrUtil.format(message, params));
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Getter for the field <code>codeArgs</code>.</p>
     *
     * @return an array of {@link java.lang.Object} objects.
     */
    public Object[] getCodeArgs() {
        return codeArgs;
    }

    /**
     * <p>buildCodeArgs.</p>
     *
     * @param codeArgs a {@link java.lang.Object} object.
     * @return a {@link com.zhituanbox.core.exception.BusinessException} object.
     */
    public BusinessException buildCodeArgs(Object... codeArgs) {
        this.codeArgs = codeArgs;
        return this;
    }
}
