package com.zhituanbox.core.exception;

import javax.validation.constraints.NotNull;

/**
 * 用户业务异常，由用户请求参数等导致的异常
 * <ul>
 *     <li>请求参数校验异常</li>
 *     <li>由用户的导致的</li>
 *     <li>可避免的</li>
 * </ul>
 *
 * @author juanb
 * @version 1.0.0
 */
public class UserBusinessException extends BusinessException {

    /**
     * <p>Constructor for UserBusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param cause a {@link java.lang.Throwable} object.
     * @param code a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     */
    public UserBusinessException(@NotNull String message, Throwable cause, String code, Object... params) {
        super(code, cause, message, params);
    }

    /**
     * <p>Constructor for UserBusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param code a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     */
    public UserBusinessException(@NotNull String message, String code, Object... params) {
        super(code, message, params);
    }
}
