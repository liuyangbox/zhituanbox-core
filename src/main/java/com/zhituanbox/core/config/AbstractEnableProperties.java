package com.zhituanbox.core.config;

/**
 * 是否启用抽象配置类
 * <p>默认为开启</p>
 *
 * @author juanb
 * @version 1.0.0
 */
public abstract class AbstractEnableProperties {
    /**
     * 是否启用
     */
    private Boolean enable = Boolean.TRUE;

    /**
     * <p>Getter for the field <code>enable</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getEnable() {
        return enable;
    }

    /**
     * <p>Setter for the field <code>enable</code>.</p>
     *
     * @param enable a {@link java.lang.Boolean} object.
     */
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
