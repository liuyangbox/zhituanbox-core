package com.zhituanbox.core.config;

/**
 * <p>BoxCoreConstant class.</p>
 *
 * @author juanb
 * @version 1.0.0
 */
public class BoxCoreConstant {

    /**
     * 纸团盒关键字前缀
     */
    public static final String BOX_PREFIX = "zhituanbox";
    /**
     * spring 配置参数基本常亮
     */
    public static final String SPRING_CONFIGURATION_PROPERTIES_PREFIX = "spring." + BOX_PREFIX;
    /**
     * 纸团盒自动核心配置参数
     */
    public static final String SPRING_CONFIGURATION_PROPERTIES_PREFIX_CORE = SPRING_CONFIGURATION_PROPERTIES_PREFIX + ".core";

    /**
     * 启用属性名
     * {@link AbstractEnableProperties#getEnable()}
     */
    public static final String ENABLE_PROPERTIES_KEY = "enable";
}
