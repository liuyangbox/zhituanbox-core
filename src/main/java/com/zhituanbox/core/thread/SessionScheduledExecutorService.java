package com.zhituanbox.core.thread;

import java.util.concurrent.ScheduledExecutorService;

/**
 * 会话传递线程池
 * <p>带调度</p>
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public interface SessionScheduledExecutorService extends ScheduledExecutorService, SessionExecutorService {

}
