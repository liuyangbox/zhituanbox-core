/**
 * 该包下，主要实现多线程会话传递
 * <ul>
 *     <li>用户会话</li>
 *     <li>其他ThreadLocal等状态信息</li>
 * </ul>
 * @author LiuYang
 * @since 2020/7/6
 */
package com.zhituanbox.core.thread;
