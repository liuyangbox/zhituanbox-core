package com.zhituanbox.core.thread.wrapper;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * An abstract {@code ScheduledExecutorService} that allows subclasses to
 * {@linkplain #wrapTask(Callable) wrap} tasks before they are submitted to the underlying executor.
 *
 * <p>Note that task wrapping may occur even if the task is never executed.
 * <p>com.google.common.util.concurrent.WrappingScheduledExecutorService</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public abstract class WrappingScheduledExecutorService extends WrappingExecutorService
        implements ScheduledExecutorService {
    final ScheduledExecutorService delegate;

    /**
     * <p>Constructor for WrappingScheduledExecutorService.</p>
     *
     * @param delegate a {@link java.util.concurrent.ScheduledExecutorService} object.
     */
    protected WrappingScheduledExecutorService(ScheduledExecutorService delegate) {
        super(delegate);
        this.delegate = delegate;
    }

    /** {@inheritDoc} */
    @Override
    public final ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return delegate.schedule(wrapTask(command), delay, unit);
    }

    /** {@inheritDoc} */
    @Override
    public final <V> ScheduledFuture<V> schedule(Callable<V> task, long delay, TimeUnit unit) {
        return delegate.schedule(wrapTask(task), delay, unit);
    }

    /** {@inheritDoc} */
    @Override
    public final ScheduledFuture<?> scheduleAtFixedRate(
            Runnable command, long initialDelay, long period, TimeUnit unit) {
        return delegate.scheduleAtFixedRate(wrapTask(command), initialDelay, period, unit);
    }

    /** {@inheritDoc} */
    @Override
    public final ScheduledFuture<?> scheduleWithFixedDelay(
            Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return delegate.scheduleWithFixedDelay(wrapTask(command), initialDelay, delay, unit);
    }
}
