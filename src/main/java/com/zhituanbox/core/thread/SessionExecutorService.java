package com.zhituanbox.core.thread;

import java.util.concurrent.ExecutorService;

/**
 * 会话传递线程池
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public interface SessionExecutorService extends ExecutorService {
}
