package com.zhituanbox.core.thread;

import com.zhituanbox.core.thread.impl.SessionExecutorServiceImpl;
import com.zhituanbox.core.thread.impl.SessionScheduledExecutorServiceImpl;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.*;

/**
 * 会话线程静态工厂类
 *
 * @author LiuYang
 * @since 2020/7/17
 * @see Executors
 * @version 1.0.0
 */
public class SessionExecutors {
    private SessionExecutors() {
    }

    /**
     * 创建CPU密集型线程池
     * <ul>
     *     <li>corePoolSize为{@link java.lang.Runtime#availableProcessors()}+1</li>
     *     <li>最大线程数量为corePoolSize*2</li>
     *     <li>队列使用ArrayBlockingQueue(corePoolSize*2)</li>
     * </ul>
     *
     * @param threadPrefix 线程前缀
     * @param sessionExecutorProcessors  会话执行处理器
     * @return 返回常用配置的线程池
     */
    @NotNull
    public static SessionExecutorService newCPUThreadPool(@Nullable String threadPrefix, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        final int corePoolSize = Runtime.getRuntime().availableProcessors() + 1;
        return newBestThreadPool(corePoolSize, threadPrefix, corePoolSize * 2, corePoolSize * 2, sessionExecutorProcessors);
    }

    /**
     * 创建IO密集型线程池
     * <ul>
     *     <li>corePoolSize为{@link java.lang.Runtime#availableProcessors()}*2</li>
     *     <li>最大线程数量为corePoolSize*2</li>
     *     <li>队列使用ArrayBlockingQueue(corePoolSize*2)</li>
     * </ul>
     *
     * @param threadPrefix 线程前缀
     * @param sessionExecutorProcessors  会话执行处理器
     * @return 返回常用配置的线程池
     */
    @NotNull
    public static SessionExecutorService newIOThreadPool(@Nullable String threadPrefix, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        final int corePoolSize = Runtime.getRuntime().availableProcessors() * 2;
        return newBestThreadPool(corePoolSize, threadPrefix, corePoolSize * 2, corePoolSize * 2, sessionExecutorProcessors);
    }

    /**
     * 创建最佳常用线程池
     * <ul>
     *     <li>线程保留30S</li>
     *     <li>队列使用ArrayBlockingQueue</li>
     *     <li>拒绝策略：超过最大线程上限 将任务回馈至发起方比如main线程</li>
     * </ul>
     *
     * @param corePoolSize 核心线程数
     * @param threadPrefix 线程前缀
     * @param maximumPoolSize 最大线程数量
     * @param queueCapacity 队列大小
     * @param sessionExecutorProcessors 会话执行处理器
     * @return 返回常用配置的线程池
     */
    @NotNull
    public static SessionExecutorService newBestThreadPool(int corePoolSize, @Nullable String threadPrefix, int maximumPoolSize, @Min(1) int queueCapacity, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        //有界队列深度
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(queueCapacity);
        //超过最大线程上限 将任务回馈至发起方比如main线程
        RejectedExecutionHandler policy = new ThreadPoolExecutor.CallerRunsPolicy();
        //构造一个线程池
        ThreadFactory threadFactory = SessionExecutors.newNamedThreadFactory(threadPrefix);
        //构建线程池
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 30, TimeUnit.SECONDS, queue, threadFactory, policy);
        return new SessionExecutorServiceImpl(threadPoolExecutor, sessionExecutorProcessors);
    }

    /**
     * <p>newFixedThreadPool.</p>
     *
     * @see Executors#newFixedThreadPool(int)
     * @param nThreads a int.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newFixedThreadPool(@Min(1) int nThreads) {
        return new SessionExecutorServiceImpl(Executors.newFixedThreadPool(nThreads));
    }

    /**
     * <p>newFixedThreadPool.</p>
     *
     * @see Executors#newFixedThreadPool(int)
     * @param nThreads a int.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newFixedThreadPool(@Min(1) int nThreads, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newFixedThreadPool(nThreads), sessionExecutorProcessors);
    }

    /**
     * <p>newFixedThreadPool.</p>
     *
     * @see Executors#newFixedThreadPool(int)
     * @param nThreads a int.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newFixedThreadPool(@Min(1) int nThreads, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newFixedThreadPool(nThreads), sessionExecutorProcessors);
    }

    /**
     * <p>newFixedThreadPool.</p>
     *
     * @see Executors#newFixedThreadPool(int, ThreadFactory)
     * @param nThreads a int.
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newFixedThreadPool(@Min(1) int nThreads, @NotNull ThreadFactory threadFactory, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads, threadFactory);
        return new SessionExecutorServiceImpl(executorService, sessionExecutorProcessors);
    }

    /**
     * <p>newFixedThreadPool.</p>
     *
     * @see Executors#newFixedThreadPool(int, ThreadFactory)
     * @param nThreads a int.
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newFixedThreadPool(@Min(1) int nThreads, @NotNull ThreadFactory threadFactory, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads, threadFactory);
        return new SessionExecutorServiceImpl(executorService, sessionExecutorProcessors);
    }


    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor()
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor() {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor());
    }

    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor()
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor(@NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor(), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor()
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor(@NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor(), sessionExecutorProcessors);
    }


    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor(@NotNull ThreadFactory threadFactory) {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor(threadFactory));
    }

    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor(@NotNull ThreadFactory threadFactory, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor(threadFactory), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadExecutor.</p>
     *
     * @see Executors#newSingleThreadExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newSingleThreadExecutor(@NotNull ThreadFactory threadFactory, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newSingleThreadExecutor(threadFactory), sessionExecutorProcessors);
    }


    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool()
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool() {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool());
    }

    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool()
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool(@NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool(), sessionExecutorProcessors);
    }

    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool()
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool(@NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool(), sessionExecutorProcessors);
    }

    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool(@NotNull ThreadFactory threadFactory) {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool(threadFactory));
    }

    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool(@NotNull ThreadFactory threadFactory, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool(threadFactory), sessionExecutorProcessors);
    }

    /**
     * <p>newCachedThreadPool.</p>
     *
     * @see Executors#newCachedThreadPool(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionExecutorService} object.
     */
    @NotNull
    public static SessionExecutorService newCachedThreadPool(@NotNull ThreadFactory threadFactory, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionExecutorServiceImpl(Executors.newCachedThreadPool(threadFactory), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor()
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor() {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor());
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor()
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor(@NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor(), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor()
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor(@NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor(), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor(@NotNull ThreadFactory threadFactory) {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor(threadFactory));
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor(@NotNull ThreadFactory threadFactory, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor(threadFactory), sessionExecutorProcessors);
    }

    /**
     * <p>newSingleThreadScheduledExecutor.</p>
     *
     * @see Executors#newSingleThreadScheduledExecutor(ThreadFactory)
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newSingleThreadScheduledExecutor(@NotNull ThreadFactory threadFactory, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newSingleThreadScheduledExecutor(threadFactory), sessionExecutorProcessors);
    }


    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int)
     * @param corePoolSize a int.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize));
    }

    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int)
     * @param corePoolSize a int.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize), sessionExecutorProcessors);
    }

    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int)
     * @param corePoolSize a int.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize), sessionExecutorProcessors);
    }

    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int, ThreadFactory)
     * @param corePoolSize a int.
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize, @NotNull ThreadFactory threadFactory) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize, threadFactory));
    }

    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int, ThreadFactory)
     * @param corePoolSize a int.
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize, @NotNull ThreadFactory threadFactory, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize, threadFactory), sessionExecutorProcessors);
    }

    /**
     * <p>newScheduledThreadPool.</p>
     *
     * @see Executors#newScheduledThreadPool(int, ThreadFactory)
     * @param corePoolSize a int.
     * @param threadFactory a {@link java.util.concurrent.ThreadFactory} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     * @return a {@link com.zhituanbox.core.thread.SessionScheduledExecutorService} object.
     */
    @NotNull
    public static SessionScheduledExecutorService newScheduledThreadPool(@Min(1) int corePoolSize, @NotNull ThreadFactory threadFactory, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        return new SessionScheduledExecutorServiceImpl(Executors.newScheduledThreadPool(corePoolSize, threadFactory), sessionExecutorProcessors);
    }

    /**
     * 创建线程工厂
     *
     * @param prefix 线程名前缀,线程名前缀 例如设置前缀为session-thread-，则线程名为session-thread-1之类。
     * @since 4.0.0
     * @return a {@link java.util.concurrent.ThreadFactory} object.
     */
    public static ThreadFactory newNamedThreadFactory(String prefix) {
        return newNamedThreadFactory(prefix, false);
    }

    /**
     * 创建线程工厂
     *
     * @param prefix 线程名前缀 例如设置前缀为session-thread-，则线程名为session-thread-1之类。
     * @param isDeamon 是否守护线程
     * @since 4.0.0
     * @return a {@link java.util.concurrent.ThreadFactory} object.
     */
    public static ThreadFactory newNamedThreadFactory(@Nullable String prefix, boolean isDeamon) {
        return new NamedThreadFactory(prefix, isDeamon);
    }
}
