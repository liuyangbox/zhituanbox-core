package com.zhituanbox.core.thread.impl;

import com.zhituanbox.core.thread.SessionExecutorProcessor;
import com.zhituanbox.core.thread.SessionExecutorService;
import com.zhituanbox.core.thread.wrapper.WrappingExecutorService;
import org.springframework.core.Ordered;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * 会话传递执行线程实现类
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public class SessionExecutorServiceImpl extends WrappingExecutorService implements SessionExecutorService {
    @NotNull
    private final List<SessionExecutorProcessor> sessionExecutorProcessors = new ArrayList<>();

    /**
     * <p>Constructor for SessionExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ExecutorService} object.
     */
    public SessionExecutorServiceImpl(@NotNull ExecutorService delegate) {
        super(delegate);
    }

    /**
     * <p>Constructor for SessionExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ExecutorService} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     */
    public SessionExecutorServiceImpl(@NotNull ExecutorService delegate, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        this(delegate);
        this.sessionExecutorProcessors.addAll(Arrays.asList(sessionExecutorProcessors));
    }

    /**
     * <p>Constructor for SessionExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ExecutorService} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     */
    public SessionExecutorServiceImpl(@NotNull ExecutorService delegate, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        this(delegate);
        this.sessionExecutorProcessors.addAll(sessionExecutorProcessors);
    }

    /** {@inheritDoc} */
    @Override
    protected <T> Callable<T> wrapTask(Callable<T> callable) {
        return () -> {
            try {
                getSessionExecutorProcessors().forEach(SessionExecutorProcessor::preHandle);
                return callable.call();
            } finally {
                getSessionExecutorProcessors().forEach(SessionExecutorProcessor::afterCompletion);
            }
        };
    }

    /**
     * 获取所有的拦截器
     *
     * @return 返回排序之后的拦截器
     */
    @NotNull
    public List<SessionExecutorProcessor> getSessionExecutorProcessors() {
        return sessionExecutorProcessors.stream().sorted(Comparator.nullsLast(Comparator.comparingInt(Ordered::getOrder))).collect(Collectors.toList());
    }
}
