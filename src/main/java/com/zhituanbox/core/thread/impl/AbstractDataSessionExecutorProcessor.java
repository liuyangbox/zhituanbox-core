package com.zhituanbox.core.thread.impl;

import com.zhituanbox.core.thread.SessionExecutorProcessor;
import org.springframework.lang.Nullable;

/**
 * 抽象数据拦截器
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public abstract class AbstractDataSessionExecutorProcessor<T> implements SessionExecutorProcessor {
    @Nullable
    private T data;

    private int order;

    /**
     * <p>Constructor for AbstractDataSessionExecutorProcessor.</p>
     *
     * @param data a T object.
     */
    public AbstractDataSessionExecutorProcessor(@Nullable T data) {
        this(data, DEFAULT_ORDER);
    }

    /**
     * <p>Constructor for AbstractDataSessionExecutorProcessor.</p>
     *
     * @param data a T object.
     * @param order a int.
     */
    public AbstractDataSessionExecutorProcessor(@Nullable T data, int order) {
        this.data = data;
        this.order = order;
    }

    /** {@inheritDoc} */
    @Override
    public int getOrder() {
        return order;
    }

    /**
     * 获取数据
     * <p>例如需要处理的数据</p>
     *
     * @return 数据
     */
    protected T getData() {
        return data;
    }

}
