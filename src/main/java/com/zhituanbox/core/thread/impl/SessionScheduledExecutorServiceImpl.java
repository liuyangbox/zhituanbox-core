package com.zhituanbox.core.thread.impl;


import com.zhituanbox.core.thread.SessionExecutorProcessor;
import com.zhituanbox.core.thread.SessionScheduledExecutorService;
import com.zhituanbox.core.thread.wrapper.WrappingScheduledExecutorService;
import org.springframework.core.Ordered;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

/**
 * 调度会话传递执行线程实现类
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public class SessionScheduledExecutorServiceImpl extends WrappingScheduledExecutorService implements SessionScheduledExecutorService {

    @NotNull
    private final List<SessionExecutorProcessor> sessionExecutorProcessors = new ArrayList<>();

    /**
     * <p>Constructor for SessionScheduledExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ScheduledExecutorService} object.
     */
    public SessionScheduledExecutorServiceImpl(@NotNull ScheduledExecutorService delegate) {
        super(delegate);
    }

    /**
     * <p>Constructor for SessionScheduledExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ScheduledExecutorService} object.
     * @param sessionExecutorProcessors a {@link com.zhituanbox.core.thread.SessionExecutorProcessor} object.
     */
    public SessionScheduledExecutorServiceImpl(@NotNull ScheduledExecutorService delegate, @NotNull SessionExecutorProcessor... sessionExecutorProcessors) {
        super(delegate);
        this.sessionExecutorProcessors.addAll(Arrays.asList(sessionExecutorProcessors));
    }

    /**
     * <p>Constructor for SessionScheduledExecutorServiceImpl.</p>
     *
     * @param delegate a {@link java.util.concurrent.ScheduledExecutorService} object.
     * @param sessionExecutorProcessors a {@link java.util.List} object.
     */
    public SessionScheduledExecutorServiceImpl(@NotNull ScheduledExecutorService delegate, @NotNull List<SessionExecutorProcessor> sessionExecutorProcessors) {
        super(delegate);
        this.sessionExecutorProcessors.addAll(sessionExecutorProcessors);
    }

    /** {@inheritDoc} */
    @Override
    protected <T> Callable<T> wrapTask(@NotNull Callable<T> callable) {
        return () -> {
            try {
                getSessionExecutorProcessors().forEach(SessionExecutorProcessor::preHandle);
                return callable.call();
            } finally {
                getSessionExecutorProcessors().forEach(SessionExecutorProcessor::afterCompletion);
            }
        };
    }

    /**
     * 获取所有的拦截器
     *
     * @return 返回排序之后的拦截器
     */
    @NotNull
    public List<SessionExecutorProcessor> getSessionExecutorProcessors() {
        return sessionExecutorProcessors.stream().sorted(Comparator.nullsLast(Comparator.comparingInt(Ordered::getOrder))).collect(Collectors.toList());
    }
}
