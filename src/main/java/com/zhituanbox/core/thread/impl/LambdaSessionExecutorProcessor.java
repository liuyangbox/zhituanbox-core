package com.zhituanbox.core.thread.impl;


import com.zhituanbox.core.thread.SessionExecutorProcessor;

import javax.validation.constraints.NotNull;

/**
 * 表达式拦截器
 *
 * @author LiuYang
 * @since 2020/7/6
 * @version 1.0.0
 */
public class LambdaSessionExecutorProcessor implements SessionExecutorProcessor {

    private int order;
    private Runnable preHandle;
    private Runnable afterCompletion;

    /**
     * <p>Constructor for LambdaSessionExecutorProcessor.</p>
     *
     * @param preHandle a {@link java.lang.Runnable} object.
     * @param afterCompletion a {@link java.lang.Runnable} object.
     * @param order a int.
     */
    public LambdaSessionExecutorProcessor(@NotNull Runnable preHandle, @NotNull Runnable afterCompletion, int order) {
        this.order = order;
        this.preHandle = preHandle;
        this.afterCompletion = afterCompletion;
    }

    /**
     * <p>Constructor for LambdaSessionExecutorProcessor.</p>
     *
     * @param preHandle a {@link java.lang.Runnable} object.
     * @param afterCompletion a {@link java.lang.Runnable} object.
     */
    public LambdaSessionExecutorProcessor(@NotNull Runnable preHandle, @NotNull Runnable afterCompletion) {
        this(preHandle, afterCompletion, DEFAULT_ORDER);
    }

    /** {@inheritDoc} */
    @Override
    public void preHandle() {
        preHandle.run();
    }

    /** {@inheritDoc} */
    @Override
    public void afterCompletion() {
        afterCompletion.run();
    }

    /** {@inheritDoc} */
    @Override
    public int getOrder() {
        return order;
    }

}
