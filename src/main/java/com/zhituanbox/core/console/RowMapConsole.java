package com.zhituanbox.core.console;

import cn.hutool.core.io.LineHandler;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 行打印-打印key，value内容
 * <pre>
 *  -------------------------------------------------------------------------
 *  | 跨域: CorsWebMvcConfigurer.class |
 *   -------------------------------------------------------------------------
 *  | dynamic         :                                                 false |
 *  | withCredentials :                                                  true |
 *  | allowedMethods  : [POST, DELETE, PUT, OPTIONS, HEAD, TRACE, GET, PATCH] |
 *  | allowedOrigins  :                                                   [*] |
 *  | allowedHeaders  :                                                   [*] |
 *  | enable          :                                                  true |
 *   -------------------------------------------------------------------------
 * </pre>
 * @author LiuYang
 * @since 2020/9/29
 */
public class RowMapConsole implements Console {
    /**
     * 标题
     */
    @NotBlank
    private String title;
    /**
     * 事项
     */
    @Nullable
    private Map<@NotBlank String, @NotBlank String> items;

    public RowMapConsole(String title, Map<String, String> items) {
        this.title = title;
        this.items = items;
    }

    @Override
    public void lineHandler(@NotNull LineHandler lineHandler) {
        List<String> lineItems = new ArrayList<>();
        if (MapUtil.isNotEmpty(items)) {
            @SuppressWarnings("OptionalGetWithoutIsPresent")
            int keyMaxLength = items.keySet().stream().mapToInt(String::length).max().getAsInt();
            int valueMaxLength = items.values().stream().mapToInt(String::length).max().getAsInt();
            items.forEach((k, v) -> {
                String row = StrUtil.padAfter(k, keyMaxLength, " ") + " : " + StrUtil.padAfter(v, valueMaxLength, " ");
                lineItems.add(row);
            });
        }
        RowListConsole console = new RowListConsole(title, lineItems);
        console.lineHandler(lineHandler);
    }

}
