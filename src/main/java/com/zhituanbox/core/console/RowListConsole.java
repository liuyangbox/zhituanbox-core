package com.zhituanbox.core.console;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.LineHandler;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.taobao.text.Color;
import com.taobao.text.Style;
import com.taobao.text.ui.BorderStyle;
import com.taobao.text.ui.RowElement;
import com.taobao.text.ui.TableElement;
import com.taobao.text.util.RenderUtil;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 行打印-打印一行内容
 * <pre>
 *    -----------------------------------------
 *  | 加载 SpringApplicationContextUtil.class |
 *   -----------------------------------------
 *  | publishEvent                            |
 *  | getBean                                 |
 *  | getApplicationContext                   |
 *   -----------------------------------------
 * </pre>
 *
 * @author LiuYang
 * @since 2020/9/29
 * @version 1.0.0
 */
public class RowListConsole implements Console {
    /**
     * 标题
     */
    @NotBlank
    private String title;
    /**
     * 事项
     */
    @Nullable
    private List<String> items;

    /**
     * <p>Constructor for RowListConsole.</p>
     *
     * @param title a {@link java.lang.String} object.
     * @param items a {@link java.util.Collection} object.
     */
    public RowListConsole(String title, Collection<String> items) {
        this.title = title;
        Assert.notBlank(title);
        this.items = CollUtil.newArrayList(items);
    }

    /** {@inheritDoc} */
    @Override
    public void lineHandler(@NotNull LineHandler lineHandler) {
        int maxLength = getMaxLength();

        TableElement tableElement = new TableElement();
        tableElement.border(BorderStyle.DASHED).separator(BorderStyle.DASHED);
        tableElement.leftCellPadding(1).rightCellPadding(1);

        tableElement.header(new RowElement().add(StrUtil.padAfter(title, maxLength, " ")).style(Style.style(Color.green)));

        maxLength = maxLength + tableElement.getLeftCellPadding() + tableElement.getRightCellPadding();

        if (Objects.nonNull(items)) {
            for (String v : items) {
                String row = StrUtil.padAfter(v, maxLength, " ");
                tableElement.row(new RowElement().add(row));
            }
        }

        String render = RenderUtil.render(tableElement);
        IoUtil.toStream(render, Charset.defaultCharset());
        IoUtil.readLines(IoUtil.toStream(render, Charset.defaultCharset()), Charset.defaultCharset(), lineHandler);
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    private int getMaxLength() {
        Collection<String> strings = CollUtil.newArrayList(items);
        strings.add(title);
        return strings.stream().filter(StrUtil::isNotEmpty).mapToInt(String::length).max().getAsInt();
    }

}
