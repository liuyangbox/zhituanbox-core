package com.zhituanbox.core.console;

import cn.hutool.core.io.LineHandler;

import javax.validation.constraints.NotNull;

/**
 * <p>Console interface.</p>
 *
 * @author LiuYang
 * @since 2020/9/30
 * @version 1.0.0
 */
public interface Console {
    /**
     * 转换之后的行处理
     *
     * @param lineHandler 每行处理器
     */
    void lineHandler(@NotNull LineHandler lineHandler);
}
