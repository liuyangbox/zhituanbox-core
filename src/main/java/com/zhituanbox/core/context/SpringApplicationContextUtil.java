package com.zhituanbox.core.context;

import cn.hutool.core.io.LineHandler;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.console.RowListConsole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>SpringApplicationContextUtil class.</p>
 *
 * @author juanb
 * @version 1.0.0
 */
public class SpringApplicationContextUtil implements ApplicationContextAware {

    private final Logger log = LoggerFactory.getLogger(SpringApplicationContextUtil.class);
    private static ApplicationContext applicationContext;

    /** {@inheritDoc} */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringApplicationContextUtil.applicationContext == null) {
            SpringApplicationContextUtil.applicationContext = applicationContext;
        }
        //打印日志
        String header = StrUtil.format("加载 {}.class", SpringApplicationContextUtil.class.getSimpleName());
        Set<String> methods = Arrays.stream(ClassUtil.getPublicMethods(SpringApplicationContextUtil.class))
                .filter(ClassUtil::isStatic)
                .map(Method::getName)
//                .map(method -> StrUtil.format("method: {}", method.getName()))
                .collect(Collectors.toSet());

        RowListConsole console = new RowListConsole(header, methods);
        LineHandler lineHandler = log::info;
        console.lineHandler(lineHandler);
    }

    /**
     * <p>Getter for the field <code>applicationContext</code>.</p>
     *
     * @return a {@link org.springframework.context.ApplicationContext} object.
     */
    @NotNull
    public static ApplicationContext getApplicationContext() {
        Assert.notNull(applicationContext);
        return applicationContext;
    }


    /**
     * <p>getBean.</p>
     *
     * @param clazz a {@link java.lang.Class} object.
     * @param <T> a T object.
     * @return a T object.
     */
    @NotNull
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /**
     * <p>getBean.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param clazz a {@link java.lang.Class} object.
     * @param <T> a T object.
     * @return a T object.
     */
    @NotNull
    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }

    /**
     * <p>publishEvent.</p>
     *
     * @param event a {@link org.springframework.context.ApplicationEvent} object.
     */
    public static void publishEvent(@NotNull ApplicationEvent event) {
        getApplicationContext().publishEvent(event);
    }
}
