/*
 * Copyright (c) 2011-2020, baomidou (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.zhituanbox.core.lambda.util;

import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.lambda.LambdaBeanWrapper;
import com.zhituanbox.core.lambda.LambdaFunction;
import com.zhituanbox.core.lambda.SerializedLambdaException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Lambda 解析工具类
 *
 * @since 2018-05-10
 * @version 1.0.0
 * @author LiuYang
 */
public final class LambdaUtils {
    /**
     * SerializedLambda 反序列化缓存
     */
    private static final Map<Class<?>, SerializedLambda> FUNC_CACHE = new WeakHashMap<>();

    /**
     * 解析 lambda 表达式
     * 该缓存可能会在任意不定的时间被清除
     *
     * @param func 需要解析的 lambda 对象
     * @param <T>  类型，被调用的 Function 对象的目标类型
     * @return 返回解析后的结果
     * @throws com.zhituanbox.core.lambda.SerializedLambdaException if any.
     */
    @NotNull
    public static <T> SerializedLambda resolve(@NotNull LambdaFunction<T, ?> func) throws SerializedLambdaException {
        Class<?> clazz = func.getClass();
        return FUNC_CACHE.computeIfAbsent(clazz, t -> resolveLambda(func));
    }

    @NotNull
    private static SerializedLambda resolveLambda(@NotNull LambdaFunction<?, ?> lambda) throws SerializedLambdaException {
        if (!lambda.getClass().isSynthetic()) {
            throw new SerializedLambdaException("该方法仅能传入 lambda 表达式产生的合成类");
        }
        try {
            // 通过获取对象方法，判断是否存在该方法
            Method method = lambda.getClass().getDeclaredMethod("writeReplace");
            method.setAccessible(Boolean.TRUE);
            // 利用jdk的SerializedLambda 解析方法引用
            return (SerializedLambda) method.invoke(lambda);
        } catch (Exception ex) {
            throw new SerializedLambdaException(ex.getMessage(), ex);
        }
    }

    /**
     * 解析bean
     *
     * @param beanClass bean对象
     * @param <T> 泛型
     * @return LambdaBeanWrapper
     */
    public static <T> LambdaBeanWrapper<T> resolveBean(@NotNull Class<T> beanClass) {
        return new LambdaBeanWrapper<>(beanClass);
    }

    /**
     * bean对象属性
     *
     * @param lambdaFunction lambda表达式；::方式
     * @see LambdaBeanWrapper#resolveProperty(LambdaFunction)
     * @return 对象属性
     * @param <T> a T object.
     */
    @NotBlank
    public static <T> String resolveProperty(@NotNull LambdaFunction<T, ?> lambdaFunction) {
        @NotNull SerializedLambda serializedLambda = resolve(lambdaFunction);
        String implClass = serializedLambda.getImplClass();
        if (StrUtil.isBlank(implClass) || StrUtil.startWith(serializedLambda.getImplMethodName(), "lambda$")) {
            //实现类为空，或者以lambda$开头
            //https://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html
            throw new SerializedLambdaException("please use ::(method reference) lambda way");
        }
        Class<T> clazz = ClassUtil.loadClass(StrUtil.replace(implClass, "/", "."));
        return resolveBean(clazz).resolveProperty(lambdaFunction);
    }
}
