package com.zhituanbox.core.lambda;

import cn.hutool.core.bean.BeanDesc;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.PropDesc;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.lambda.util.LambdaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 通过Lambda表达式来获取Bean对象属性名
 *
 * <ul>
 *     <li>会校验是否为Bean对象{@link cn.hutool.core.bean.BeanUtil#isBean(Class)}</li>
 * </ul>
 *
 * @author LiuYang
 * @since 2020/8/20
 * @version 1.0.0
 */
public class LambdaBeanWrapper<T> {
    private final Logger log = LoggerFactory.getLogger(LambdaBeanWrapper.class);

    private final static String tips = "tips: get property \"id\" LambdaFunction func = Entity::getId";

    @NotNull
    private final BeanDesc beanDesc;


    /**
     * <p>Constructor for LambdaBeanWrapper.</p>
     *
     * @param beanClass a {@link java.lang.Class} object.
     */
    public LambdaBeanWrapper(@NotNull Class<T> beanClass) {
        Assert.notNull(beanClass);
        BeanUtil.isBean(beanClass);
        this.beanDesc = BeanUtil.getBeanDesc(beanClass);
    }

    /**
     * 获取属性名
     *
     * @see LambdaBeanWrapper#tips
     * @param lambdaFunction 双:: 方式的表达式
     * @return bean的属性名
     */
    @NotBlank
    public String resolveProperty(@NotNull LambdaFunction<T, ?> lambdaFunction) {
        return resolveField(lambdaFunction).getName();
    }

    /**
     * 获取bean字段
     *
     * @see LambdaBeanWrapper#tips
     * @param lambdaFunction 双:: 方式的表达式
     * @return bean的属性名
     */
    @NotNull
    public Field resolveField(@NotNull LambdaFunction<T, ?> lambdaFunction) {
        @NotNull SerializedLambda serializedLambda = LambdaUtils.resolve(lambdaFunction);
        String methodName = serializedLambda.getImplMethodName();
        log.debug("resolve lambda function: {}::{}", beanDesc.getSimpleName(), methodName);
        //先属性全匹配获取
        PropDesc prop = beanDesc.getProp(methodName);
        log.debug("Bean[{}] match field: {}", beanDesc.getName(), methodName);
        if (Objects.isNull(prop)) {
            log.debug("Bean[{}] match getter and setter method: {}", beanDesc.getName(), methodName);
            //尝试匹配属性的getter setter方法，例如name 匹配 getName
            prop = CollUtil.findOne(beanDesc.getProps(), propDesc -> {
                Predicate<Method> match = method -> Optional.ofNullable(method)
                        .filter(t -> Objects.equals(t.getName(), methodName))
                        .isPresent();
                return match.test(propDesc.getGetter()) || match.test(propDesc.getSetter());
            });
        }
        if (Objects.isNull(prop)) {
            log.error("Bean[{}] not found field: {}", beanDesc.getName(), methodName);
            log.error(tips);
            throw new SerializedLambdaException(StrUtil.format("{}#{} resolve failure; please use ::(method reference) lambda way", beanDesc.getName(), methodName));
        }
        return prop.getField();
    }

}
