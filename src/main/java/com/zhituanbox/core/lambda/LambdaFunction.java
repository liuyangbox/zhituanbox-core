package com.zhituanbox.core.lambda;

import java.io.Serializable;
import java.util.function.Function;

/**
 *支持序列化的 Function
 *
 * @author LiuYang
 * @since 2020/8/20
 * @version 1.0.0
 */
@FunctionalInterface
public interface LambdaFunction<T, R> extends Function<T, R>, Serializable {
}
