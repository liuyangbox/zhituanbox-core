package com.zhituanbox.core.lambda;

/**
 * 序列化Lambda表达式异常
 *
 * @author LiuYang
 * @since 2020/8/20
 * @version 1.0.0
 */
public class SerializedLambdaException extends RuntimeException{
    /**
     * <p>Constructor for SerializedLambdaException.</p>
     */
    public SerializedLambdaException() {
        super();
    }

    /**
     * <p>Constructor for SerializedLambdaException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public SerializedLambdaException(String message) {
        super(message);
    }

    /**
     * <p>Constructor for SerializedLambdaException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param throwable a {@link java.lang.Throwable} object.
     */
    public SerializedLambdaException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * <p>Constructor for SerializedLambdaException.</p>
     *
     * @param throwable a {@link java.lang.Throwable} object.
     */
    public SerializedLambdaException(Throwable throwable) {
        super(throwable);
    }
}
