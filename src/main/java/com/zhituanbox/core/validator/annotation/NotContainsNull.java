package com.zhituanbox.core.validator.annotation;

import com.zhituanbox.core.validator.constraint.ArrayNotContainsNullConstraintValidator;
import com.zhituanbox.core.validator.constraint.CollectionNotContainsNullConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 不能包含null值
 * <p>支持数组,集合类型，如果为null，则不进行校验</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER, CONSTRUCTOR, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = {ArrayNotContainsNullConstraintValidator.class, CollectionNotContainsNullConstraintValidator.class})
@Documented
public @interface NotContainsNull {
    String message() default "{box.validator.constraints.contains.null}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
