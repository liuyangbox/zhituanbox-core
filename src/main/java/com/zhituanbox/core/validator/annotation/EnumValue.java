package com.zhituanbox.core.validator.annotation;

import com.zhituanbox.core.enums.IBaseEnum;
import com.zhituanbox.core.validator.constraint.EnumConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 限定值为枚举值
 * <p>支持任一类型，如果为null，则不进行校验</p>
 * <p>枚举必须实现{@link com.zhituanbox.core.enums.IBaseEnum}</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER, CONSTRUCTOR, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = {EnumConstraintValidator.class})
@Documented
public @interface EnumValue {

    String message() default "{box.validator.constraints.enum}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 需要校验值的枚举
     * @return 需要校验值的枚举
     */
    Class<? extends java.lang.Enum<? extends IBaseEnum<?>>> value();

    /**
     * 只比较本身
     * <p>类似集合，数组，是比较里面的值，还是集合本身，因为有可能枚举的value就是集合。因此需要设置为true</p>
     * <p>默认会处理集合或数组里面的值</p>
     * 默认false
     * @return 是否只比较本身
     */
    boolean equalsRoot() default false;
}
