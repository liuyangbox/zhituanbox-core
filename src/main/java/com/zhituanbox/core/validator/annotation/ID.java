package com.zhituanbox.core.validator.annotation;

import com.zhituanbox.core.validator.constraint.IntIDConstraintValidator;
import com.zhituanbox.core.validator.constraint.LongIDConstraintValidator;
import com.zhituanbox.core.validator.constraint.StringIDConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 主键参数校验
 * <ul>
 *     <li>支持Long、Integer、String</li>
 *     <li>只有不为null时，才会校验</li>
 *     <li>不能包含空格</li>
 *     <li>内容默认为正整数</li>
 *     <li>可配置为包含数字和字母的数据</li>
 * </ul>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = {LongIDConstraintValidator.class, IntIDConstraintValidator.class, StringIDConstraintValidator.class})
@Documented
public @interface ID {

    String message() default "{box.validator.constraints.id}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 小写字母
     * 默认可包含小写字母
     * <p><b>只有String类型才生效</b></p>
     * @return 是否允许小写字母
     */
    boolean lowerCase() default true;

    /**
     * 大写字母
     * 默认可包含大写字母
     * <p><b>只有String类型才生效</b></p>
     * @return 是否允许大写字母
     */
    boolean upperCase() default true;
}
