package com.zhituanbox.core.validator.annotation;

import com.zhituanbox.core.validator.constraint.NameConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 名称校验
 * <p>不为null并且长度大于0，才会生效校验；可以搭配@NotBlank一起使用</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = NameConstraintValidator.class)
@Documented
public @interface Name {

    String message() default "{box.validator.constraints.name}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 允许开头有空格
     * 默认：false（不允许）
     * @return 是否允许结尾有空格
     */
    boolean startBlank() default false;

    /**
     * 允许结尾有空格
     * 默认：false（不允许）
     * @return 是否允许结尾有空格
     */
    boolean endBlank() default false;

    /**
     * 允许包含空格
     * 默认：false（不允许）
     * @return 是否允许包含空格
     */
    boolean containsBlank() default false;
}
