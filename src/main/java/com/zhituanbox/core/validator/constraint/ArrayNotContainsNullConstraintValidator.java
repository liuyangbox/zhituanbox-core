package com.zhituanbox.core.validator.constraint;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.validator.annotation.Name;
import com.zhituanbox.core.validator.annotation.NotContainsNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * <p>ArrayNotContainsNullConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class ArrayNotContainsNullConstraintValidator implements ConstraintValidator<NotContainsNull, Object[]> {
    private final static Logger log = LoggerFactory.getLogger(ArrayNotContainsNullConstraintValidator.class);
//    private NotContainsNull constraintAnnotation;

    /** {@inheritDoc} */
    @Override
    public void initialize(NotContainsNull constraintAnnotation) {
//        this.constraintAnnotation = constraintAnnotation;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Object[] value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        return !ArrayUtil.hasNull(value);
    }
}
