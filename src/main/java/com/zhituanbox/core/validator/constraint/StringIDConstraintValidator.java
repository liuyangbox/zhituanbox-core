package com.zhituanbox.core.validator.constraint;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.validator.annotation.ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;

/**
 * 字符串主键，适用于适用UUID等
 * <p>e54c999b302c411c9324e5795c62f63a</p>
 * <p>都开启的情况下，只能包含数字大小写字母</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class StringIDConstraintValidator implements ConstraintValidator<ID, String> {
    private final static Logger log = LoggerFactory.getLogger(StringIDConstraintValidator.class);
    @NotNull
    private ID constraintAnnotation;

    /** {@inheritDoc} */
    @Override
    public void initialize(ID constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StrUtil.isEmpty(value)) {
            return true;
        }
        StringBuilder containsWords = new StringBuilder(RandomUtil.BASE_NUMBER);
        //允许小写字母
        if (constraintAnnotation.lowerCase()) {
            containsWords.append(RandomUtil.BASE_CHAR);
        }
        //允许大写字母
        if (constraintAnnotation.upperCase()) {
            containsWords.append(RandomUtil.BASE_CHAR.toUpperCase());
        }

        return StrUtil.containsOnly(value, containsWords.toString().toCharArray());
    }
}
