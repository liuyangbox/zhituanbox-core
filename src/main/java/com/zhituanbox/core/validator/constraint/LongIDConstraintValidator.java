package com.zhituanbox.core.validator.constraint;

import cn.hutool.core.util.NumberUtil;
import com.zhituanbox.core.validator.annotation.ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * <p>LongIDConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class LongIDConstraintValidator implements ConstraintValidator<ID, Long> {
    private final static Logger log = LoggerFactory.getLogger(LongIDConstraintValidator.class);

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        //需要大于0
        return NumberUtil.compare(value, 0) >= 1;
    }
}
