package com.zhituanbox.core.validator.constraint;

import cn.hutool.cache.Cache;
import cn.hutool.cache.impl.WeakCache;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.IterUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.TypeUtil;
import com.zhituanbox.core.enums.IBaseEnum;
import com.zhituanbox.core.validator.annotation.EnumValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import javax.validation.ConstraintDefinitionException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * <p>EnumConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class EnumConstraintValidator implements ConstraintValidator<EnumValue, Object> {
    private final static Logger log = LoggerFactory.getLogger(EnumConstraintValidator.class);

    /**
     * 泛型类型，默认为Object类型
     */
    private Type genericType = Object.class;

    private EnumValue constraintAnnotation;
    /**
     * 枚举类与其泛型的缓存
     */
    private final Cache<Class<? extends Enum<? extends IBaseEnum<?>>>, Type> containsEnumClassGenericTypeCache = new WeakCache<>(DateUnit.HOUR.getMillis());

    /** {@inheritDoc} */
    @Override
    public void initialize(EnumValue constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
        Class<? extends Enum<? extends IBaseEnum<?>>> containsEnumClass = constraintAnnotation.value();
        if (!containsEnumClass.isEnum()) {
            String s = "ContainsEnum.value必须为枚举";
            log.error("{}: {}", containsEnumClass, s);
            throw new ConstraintDefinitionException(s);
        }
        Enum<? extends IBaseEnum<?>>[] enumConstants = containsEnumClass.getEnumConstants();
        if (ArrayUtil.isEmpty(enumConstants)) {
            String s = "ContainsEnum.value枚举必须有成员";
            log.error("{}: {}", containsEnumClass, s);
            throw new ConstraintDefinitionException(s);
        }

        //获取枚举的泛型类型
        getBaseEnumGenericType(containsEnumClass).ifPresent(t -> {
            genericType = t;
            debug("泛型读取成功");
        });
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        if (Objects.isNull(value)) {
            return true;
        }
        final Class<?> clazz = value.getClass();
        boolean equalsRoot = constraintAnnotation.equalsRoot();
        if (!equalsRoot) {
            if (ClassUtil.isAssignable(Collection.class, clazz)) {
                debug("匹配集合内容");
                return isValidCollection((Collection<?>) value);
            } else if (ArrayUtil.isArray(value)) {
                debug("匹配数组内容");
                return isValidArray((Object[]) value);
            }
        }
        return isValidRoot(value);
    }

    /**
     * 校验数组里的值
     * @param array 集合值
     * @return true：校验通过
     */
    private boolean isValidArray(Object[] array) {
        return isValidIterable(CollUtil.newArrayList(array), "数组");
    }

    /**
     * 校验集合里的值
     * @param collection 集合值
     * @return true：校验通过
     */
    private boolean isValidCollection(Collection<?> collection) {
        return isValidIterable(collection, "集合");
    }

    /**
     * 校验集合里的值
     * @param iterable 集合值
     * @param iterableType 迭代类型
     * @return true：校验通过
     */
    private boolean isValidIterable(Iterable<?> iterable, String iterableType) {
        List<?> values = CollUtil.newArrayList(iterable);
        for (int i = 0; i < values.size(); i++) {
            Object value = CollUtil.get(values, i);
            if (Objects.nonNull(value)) {
                if (!ClassUtil.isAssignable(TypeUtil.getClass(genericType), value.getClass())) {
                    debug("{}值类型[{}]索引[{}]不匹配", iterableType, value.getClass().getSimpleName(), i);
                    return false;
                }
            }
            Class<?> clazz = value.getClass();
            Optional<Enum<? extends IBaseEnum<?>>> enumConstantOpt = findEnumConstant(value);
            if (enumConstantOpt.isPresent()) {
                Enum<? extends IBaseEnum<?>> enumConstant = enumConstantOpt.get();
                debug("{}值类型[{}]索引[{}]匹配到枚举值[{}]", iterableType, clazz.getSimpleName(), i, enumConstant.name());
            } else {
                debug("{}值类型[{}]索引[{}]未匹配到枚举值", iterableType, clazz.getSimpleName(), i);
                return false;
            }
        }
        return true;
    }

    /**
     * 校验值本身
     * @param value 值
     * @return 数据对象
     */
    private boolean isValidRoot(@NotNull Object value) {
        final Class<?> clazz = value.getClass();
        if (!ClassUtil.isAssignable(TypeUtil.getClass(genericType), clazz)) {
            debug("值类型[{}]不匹配", clazz.getSimpleName());
            return false;
        }
        Optional<Enum<? extends IBaseEnum<?>>> enumConstantOpt = findEnumConstant(value);
        enumConstantOpt.ifPresent(enumConstant -> debug("值类型[{}]匹配到枚举值[{}]", clazz.getSimpleName(), enumConstant.name()));
        return enumConstantOpt.isPresent();
    }

    /**
     * 获取枚举值
     * @param value 枚举value值
     * @return EnumConstant
     */
    private Optional<Enum<? extends IBaseEnum<?>>> findEnumConstant(@Nullable Object value) {
        Enum<? extends IBaseEnum<?>>[] enumConstants = constraintAnnotation.value().getEnumConstants();
        for (Enum<? extends IBaseEnum<?>> enumConstant : enumConstants) {
            //noinspection unchecked
            IBaseEnum<Object> iBaseEnum = (IBaseEnum<Object>) enumConstant;
            boolean contains = iBaseEnum.equalsValue(value);
            if (contains) {
                return Optional.of(enumConstant);
            }
        }
        return Optional.empty();
    }

    private void debug(String format, Object... params) {
        Class<? extends Enum<? extends IBaseEnum<?>>> containsEnumClass = constraintAnnotation.value();
        log.debug("[enum {} implements IBaseEnum<{}>]: {}", containsEnumClass.getSimpleName(), TypeUtil.getClass(genericType).getSimpleName(), StrUtil.format(format, params));
    }

    /**
     * 获取{@link IBaseEnum}的泛型值
     * <pre>
     *     public enum TestBaseEnum implements IBaseEnum<Integer>{}
     *     getBaseEnumGenericType(TestBaseEnum.class) 返回 Integer.class
     * </pre>
     * @param containsEnum IBaseEnum枚举
     * @return 泛型对象
     */
    private Optional<Type> getBaseEnumGenericType(@NotNull Class<? extends Enum<? extends IBaseEnum<?>>> containsEnum) {
        Type type = containsEnumClassGenericTypeCache.get(containsEnum);
        if (type != null) {
            return Optional.of(type);
        }
        Type genericInterface = containsEnum;
        do {
            for (Type anInterface : TypeUtil.getClass(genericInterface).getGenericInterfaces()) {
                if (ClassUtil.isAssignable(IBaseEnum.class, TypeUtil.getClass(anInterface))) {
                    genericInterface = anInterface;
                }
                if (IBaseEnum.class.equals(TypeUtil.getClass(anInterface))) {
                    genericInterface = anInterface;
                    break;
                }
            }
        } while (!IBaseEnum.class.equals(TypeUtil.getClass(genericInterface)));
        if (genericInterface instanceof ParameterizedType) {
            Optional<Type> typeOptional = Optional.ofNullable(TypeUtil.getTypeArgument(genericInterface));
            typeOptional.ifPresent(t -> containsEnumClassGenericTypeCache.put(containsEnum, t));
            return typeOptional;
        }
        return Optional.empty();
    }
}
