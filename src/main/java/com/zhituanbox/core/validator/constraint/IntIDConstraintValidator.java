package com.zhituanbox.core.validator.constraint;

import com.zhituanbox.core.validator.annotation.ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * <p>IntIDConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class IntIDConstraintValidator implements ConstraintValidator<ID, Integer> {
    private final static Logger log = LoggerFactory.getLogger(IntIDConstraintValidator.class);
    @NotNull
    private LongIDConstraintValidator longIDConstraintValidator;

    /** {@inheritDoc} */
    @Override
    public void initialize(ID constraintAnnotation) {
        this.longIDConstraintValidator = new LongIDConstraintValidator();
        this.longIDConstraintValidator.initialize(constraintAnnotation);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return longIDConstraintValidator.isValid(Objects.isNull(value) ? null : Long.valueOf(value), context);
    }
}
