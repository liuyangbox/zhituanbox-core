package com.zhituanbox.core.validator.constraint;

import cn.hutool.core.util.StrUtil;
import com.zhituanbox.core.validator.annotation.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.function.Predicate;

/**
 * <p>NameConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class NameConstraintValidator implements ConstraintValidator<Name, String> {
    private final static Logger log = LoggerFactory.getLogger(NameConstraintValidator.class);
    private Name constraintAnnotation;

    /** {@inheritDoc} */
    @Override
    public void initialize(Name constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StrUtil.isEmpty(value)) {
            return true;
        }

        Predicate<String> isValid = s -> !constraintAnnotation.containsBlank() && StrUtil.containsAny(s, StrUtil.SPACE);
        isValid = isValid.or(s -> !constraintAnnotation.startBlank() && StrUtil.startWith(s, StrUtil.SPACE));
        isValid = isValid.or(s -> !constraintAnnotation.endBlank() && StrUtil.endWith(s, StrUtil.SPACE));
        return isValid.negate().test(value);
    }
}
