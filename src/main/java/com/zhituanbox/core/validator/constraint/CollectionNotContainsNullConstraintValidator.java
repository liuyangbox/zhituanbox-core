package com.zhituanbox.core.validator.constraint;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.zhituanbox.core.validator.annotation.NotContainsNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;
import java.util.Objects;

/**
 * <p>CollectionNotContainsNullConstraintValidator class.</p>
 *
 * @author LiuYang
 * @since 2020/9/25
 * @version 1.0.0
 */
public class CollectionNotContainsNullConstraintValidator implements ConstraintValidator<NotContainsNull, Collection> {
    private final static Logger log = LoggerFactory.getLogger(CollectionNotContainsNullConstraintValidator.class);
//    private NotContainsNull constraintAnnotation;

    /** {@inheritDoc} */
    @Override
    public void initialize(NotContainsNull constraintAnnotation) {
//        this.constraintAnnotation = constraintAnnotation;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(Collection value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        return !CollUtil.hasNull(value);
    }
}
