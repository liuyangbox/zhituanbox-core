package com.zhituanbox.core.sort.util;

import cn.hutool.core.lang.Assert;
import com.zhituanbox.core.sort.DichotomyConfig;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 计算数据库排序的二分法排序
 * <p>支持正整数</p>
 *
 * @author LiuYang
 * @version 1.0.0
 */
public class DichotomyUtil {
    private DichotomyUtil() {

    }

    /**
     * 计算排序值
     *
     * @param step 每次扩容阈值，最小为1
     * @param config 配置
     * @return 当前对象排序值
     */
    public static int compute(@Min(1) final int step, @NotNull DichotomyConfig config) {
        //noinspection ConstantConditions
        Assert.state(step > 0, "扩容阈值必须大于0");
        AtomicInteger sequence = new AtomicInteger();
        @NotNull Optional<Integer> prevSequence = config.getPrevSequence();
        if (prevSequence.isPresent()) {
            @NotNull Optional<Integer> nextSequence = config.getNextSequence();
            if (nextSequence.isPresent()) {
                //当前存在上一个和下一个，对二者二分取值，需要判断是否能二分取值，间隙不允许二分则扩容之后再二分
                AtomicInteger next = new AtomicInteger(nextSequence.get());
                //将要生成的优先级放置到两者中间，二分法取值.如果间隔小于2，则说明不能二分，进行扩容之后再二分
                if ((next.get() - prevSequence.get()) < 2) {
                    config.incrementSequence(next.getAndAdd(step), step);
                }
                //进行二分取值
                sequence.set(next.addAndGet(prevSequence.get()) / 2);
            } else {
                //不存在下一个，说明当前追加到结尾
                sequence.addAndGet(prevSequence.get());
                sequence.addAndGet(step);
            }
        } else {
            //不存在上一个,则放在第一个
            AtomicInteger minSequence = new AtomicInteger(step);
            @NotNull Optional<Integer> firstSequence = config.getFirstSequence();
            firstSequence.ifPresent(t -> {
                if (t < 1) {
                    config.incrementSequence(t, step);
                    minSequence.addAndGet(t);
                } else {
                    minSequence.set(t);
                }
            });
            sequence.set(minSequence.get() / 2);
        }
        return sequence.get();
    }

    /**
     * 计算排序值
     * <p>step=100</p>
     *
     * @param config 配置
     * @return 当前对象排序值
     */
    public static int compute(DichotomyConfig config) {
        return compute(100, config);
    }


}
