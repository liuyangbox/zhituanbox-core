package com.zhituanbox.core.sort;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * <p>DichotomyConfig interface.</p>
 *
 * @author juanb
 * @version 1.0.0
 */
public interface DichotomyConfig {
    /**
     * 获取上一个排序序号
     *
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getPrevSequence();

    /**
     * 获取下一个排序序号
     *
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getNextSequence();

    /**
     * 获取第一个排序序号
     *
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getFirstSequence();

    /**
     * 增量扩容-将指定序号及其后面的序号都扩容指定数值(再原基础上再加step)
     *
     * @param sequence 序号
     * @param step a int.
     */
    void incrementSequence(@NotNull Integer sequence, @Min(1) int step);
}
