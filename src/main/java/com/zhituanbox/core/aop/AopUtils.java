package com.zhituanbox.core.aop;

import cn.hutool.core.exceptions.ExceptionUtil;
import org.springframework.aop.framework.AdvisedSupport;
import org.springframework.aop.framework.AopProxy;

import java.lang.reflect.Field;

/**
 * <p>Abstract AopUtils class.</p>
 *
 * @author juanb
 * @version 1.0.0
 */
public abstract class AopUtils extends org.springframework.aop.support.AopUtils {
    /**
     * 获取代理目标的实现类
     *
     * @param proxy 代理对象
     * @return 代理接口的实现类
     */
    public static Class<?> getTargetImplClass(Object proxy) {
        if (!AopUtils.isAopProxy(proxy)) {
            //不是代理对象
            return proxy.getClass();
        }
        try {
            if (AopUtils.isJdkDynamicProxy(proxy)) {
                return getJdkDynamicProxyTargetObject(proxy).getClass();
            } else { //cglib
                return getCglibProxyTargetObject(proxy).getClass();
            }
        } catch (Exception ex) {
            throw ExceptionUtil.wrapRuntime(ex);
        }

    }

    private static Object getCglibProxyTargetObject(Object proxy) throws Exception {
        Field h = proxy.getClass().getDeclaredField("CGLIB$CALLBACK_0");
        h.setAccessible(true);
        Object dynamicAdvisedInterceptor = h.get(proxy);

        Field advised = dynamicAdvisedInterceptor.getClass().getDeclaredField("advised");
        advised.setAccessible(true);

        Object target = ((AdvisedSupport) advised.get(dynamicAdvisedInterceptor)).getTargetSource().getTarget();

        return target;
    }


    private static Object getJdkDynamicProxyTargetObject(Object proxy) throws Exception {
        Field h = proxy.getClass().getSuperclass().getDeclaredField("h");
        h.setAccessible(true);
        AopProxy aopProxy = (AopProxy) h.get(proxy);

        Field advised = aopProxy.getClass().getDeclaredField("advised");
        advised.setAccessible(true);

        Object target = ((AdvisedSupport) advised.get(aopProxy)).getTargetSource().getTarget();

        return target;
    }
}
