package com.zhituanbox.core;

import com.zhituanbox.core.config.BoxCoreConstant;
import com.zhituanbox.core.config.BoxCoreProperties;
import com.zhituanbox.core.context.SpringApplicationContextUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>BoxCoreAutoConfiguration class.</p>
 *
 * @author juanb
 * @version 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties
@ConditionalOnProperty(prefix = BoxCoreConstant.SPRING_CONFIGURATION_PROPERTIES_PREFIX_CORE, value = BoxCoreConstant.ENABLE_PROPERTIES_KEY, havingValue = "true", matchIfMissing = true)
public class BoxCoreAutoConfiguration {


    /**
     * <p>boxCoreProperties.</p>
     *
     * @return a {@link com.zhituanbox.core.config.BoxCoreProperties} object.
     */
    @Bean
    @ConfigurationProperties(prefix = BoxCoreConstant.SPRING_CONFIGURATION_PROPERTIES_PREFIX_CORE)
    public BoxCoreProperties boxCoreProperties() {
        return new BoxCoreProperties();
    }

    /**
     * <p>loadSpringApplicationContext.</p>
     *
     * @return a {@link com.zhituanbox.core.context.SpringApplicationContextUtil} object.
     */
    @Bean
    @ConditionalOnMissingBean
    public SpringApplicationContextUtil loadSpringApplicationContext() {
        return new SpringApplicationContextUtil();
    }
}
