package com.zhituanbox.validator;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.MessageSourceResourceBundleLocator;

import javax.validation.*;
import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author LiuYang
 * @since 2020/9/25
 */
public abstract class AbstractCustomValidatorTest {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @NotNull
    private Validator validator;

    @Before
    public void init() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.displayName());
        messageSource.setBasenames("messages","ValidationMessages");
        MessageInterpolator messageInterpolator = new ResourceBundleMessageInterpolator(new MessageSourceResourceBundleLocator(messageSource), null, false);
        validator = Validation.buildDefaultValidatorFactory()
                .usingContext().messageInterpolator(messageInterpolator)
                .getValidator();
//        validator = ValidationAutoConfiguration.defaultValidator();
    }

    protected Set<String> validate(@NotNull Object t) throws RuntimeException {
        Set<ConstraintViolation<@NotNull Object>> validate = validator.validate(t);
        Set<String> messages = new LinkedHashSet<>();
        for (ConstraintViolation<Object> objectConstraintViolation : validate) {
            log.error(objectConstraintViolation.toString());
            messages.add(objectConstraintViolation.getMessage());
        }
        return messages;
    }

    /**
     * 应该校验成功
     */
    protected void validateTrue(@NotNull Object t) {
        Set<String> messages = validate(t);
        if (!messages.isEmpty()) {
            throw new RuntimeException(CollUtil.join(messages, "\n"));
        }
    }

    /**
     * 应该校验失败
     */
    protected void validateFalse(@NotNull Object t) {
        Set<String> messages = validate(t);
        //结果应该不为空，断言结果为空
        Assert.isFalse(messages.isEmpty(), "断言失败: {}", CollUtil.join(messages, "\n"));
    }
}
