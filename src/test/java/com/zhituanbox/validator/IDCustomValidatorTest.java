package com.zhituanbox.validator;

import com.zhituanbox.core.validator.annotation.ID;
import com.zhituanbox.core.validator.annotation.Name;
import org.junit.Test;

/**
 * @author LiuYang
 * @since 2020/9/25
 * @see Name
 */
public class IDCustomValidatorTest extends AbstractCustomValidatorTest {


    @Test
    public void idTest() {
        validateFalse(new UserTest(123L, "1234-"));
    }

    private static class UserTest {
        @ID
        private Long idLong;
        @ID
        private long aLong;
        @ID
        private Integer idInt;
        @ID
        private String idStr;

        public UserTest(Long idLong, String idStr) {
            this.idLong = idLong;
            this.idStr = idStr;
        }
    }
}
