package com.zhituanbox.validator;

import cn.hutool.core.collection.CollUtil;
import com.zhituanbox.core.enums.IBaseEnum;
import com.zhituanbox.core.validator.annotation.EnumValue;
import com.zhituanbox.core.validator.annotation.Name;
import org.junit.Test;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author LiuYang
 * @since 2020/9/25
 * @see Name
 */
public class EnumCustomValidatorTest extends AbstractCustomValidatorTest {


    @Test
    public void test() {
        validateTrue(new UserTest(0, CollUtil.newHashSet(0, 1, 2), new Integer[]{0, 1,}));
    }

    private static class UserTest {

        @EnumValue(NumberEnum.class)
        private Integer id;

        @EnumValue(NumberEnum.class)
        private Set<Integer> idSet;

        @EnumValue(NumberEnum.class)
        private Integer[] idArray;

        public UserTest(Integer id, Set<Integer> idSet, Integer[] idArray) {
            this.id = id;
            this.idSet = idSet;
            this.idArray = idArray;
        }
    }

    public static enum BadEnum {
        bad
    }

    public static enum NumberEnum implements IBaseEnum<Integer> {
        one, two, three;


        @Override
        public Integer getValue() {
            return this.ordinal();
        }

        @Override
        public String getDesc() {
            return this.name();
        }
    }
}
