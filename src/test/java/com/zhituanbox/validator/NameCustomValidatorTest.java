package com.zhituanbox.validator;

import com.zhituanbox.core.validator.annotation.Name;
import org.junit.Test;

/**
 * @author LiuYang
 * @since 2020/9/25
 * @see com.zhituanbox.core.validator.annotation.Name
 */
public class NameCustomValidatorTest extends AbstractCustomValidatorTest {


    @Test
    public void nameTest() {
        validateFalse(new UserTest("userName-1 "));
        validateTrue(new UserTest("userName-1"));
    }

    private static class UserTest {
        @Name()
        private String name;

        public UserTest(String name) {
            this.name = name;
        }
    }
}
