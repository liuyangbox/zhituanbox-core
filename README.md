# `zhituanbox-core`
不参与具体业务的核心包。
主要包括但不限于以下：
1. SpringApplicationContextUtil
2. IBaseEnum
3. LambdaUtils
4. DichotomyUtil
5. 自定义一些注解
    - EnumValue
    - ID
    - Name
    - NotContainsNull
6. 支持会话传递的数据库线程池`SessionExecutors`

# 1.Start
```xml
<dependency>
    <groupId>com.zhituanbox</groupId>
    <artifactId>zhituanbox-core</artifactId>
    <version>{version}</version>
</dependency>
```
## 1.1.配置
### spring application.properties
```yaml
# 是否开启core，默认开启
spring.zhituanbox.core: ture
```
## 1.2.SpringApplicationContextUtil
`spring.zhituanbox.core`为`true`时自动加载
控制台打印日志如下
```verilog
22:31:28.141 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil -  -------------------------------------                                          
22:31:28.144 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil - | loaded SpringApplicationContextUtil |                                         
22:31:28.144 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil -  -------------------------------------                                          
22:31:28.144 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil - | getApplicationContext               |                                         
22:31:28.144 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil - | getBean                             |                                         
22:31:28.144 [main] INFO com.zhituanbox.core.context.SpringApplicationContextUtil -  -------------------------------------  
```

## 1.3.IBaseEnum
枚举基础值，业务当中的枚举值，都继承该接口，达到枚举值统一化，便于后续管理开发。

> 后续的自定义校验器`EnumValue`也会用到

## 1.3.1.示例
```java
 public enum NumberEnum implements IBaseEnum<Integer> {
        one, two, three;


        @Override
        public Integer getValue() {
            return this.ordinal();
        }

        @Override
        public String getDesc() {
            return this.name();
        }
    }
```

## 1.4.LambdaUtils
获取优雅bean对象的字段名而减少魔法字符串的使用
```java
private static class UserTest {

        
        private Integer id;

       
        private String name;

        public Integer getId(){
            return id;
        }

        public String getName(){
            return name;
        }
    }
```

```
LambdaUtils.resolveProperty(UserTest::getId); //id
LambdaUtils.resolveProperty(UserTest::getName); //name

LambdaBeanWrapper<UserTest>  lambdaBeanWrapper = LambdaUtils.resolveBean(UserTest.class);
lambdaBeanWrapper.resolveProperty(UserTest::getId); //id
lambdaBeanWrapper.resolveProperty(UserTest::getName); //name

```

## 1.5.DichotomyUtil
主要用于代码中的二分法排序，将逻辑抽象出来，业务方只需要告知排序值，即可算出当前排序值。

> 主要处理接口`DichotomyConfig`

### 1.5.1.DichotomyConfig
```java
public interface DichotomyConfig {
    /**
     * 获取上一个排序序号
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getPrevSequence();

    /**
     * 获取下一个排序序号
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getNextSequence();

    /**
     * 获取第一个排序序号
     * @return 不存在时，返回empty
     */
    @NotNull
    Optional<Integer> getFirstSequence();

    /**
     * 增量扩容-将指定序号及其后面的序号都扩容指定数值(再原基础上再加step)
     * @param sequence 序号
     */
    void incrementSequence(@NotNull Integer sequence, @Min(1) int step);
}

```

## 1.6.自定义`validator`注解
实现一些常用业务下的注解，减少代码中的通用逻辑书写
> 以下注解，都是在校验值为非`null`的情况下进行校验，言外之意就是当值为null时，不开启校验，因此可搭配`@NotNull`使用

### 1.6.1.`@EnumValue` 校验参数只能为枚举中的值
> 支持任意类型，但通常为[`集合(Collection)`,`数组（Array）`,`基本类型(cn.hutool.core.util.ClassUtil#isBasicType)`]
需要值不为`null`的情况下，才进行枚举校验，如果可以搭配`@NotNull`一起使用

```java
/**
 * 限定值为枚举值
 * <p>支持任一类型，如果为null，则不进行校验</p>
 * <p>枚举必须实现{@link IBaseEnum}</p>
 * @author LiuYang
 * @since 2020/9/25
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {EnumConstraintValidator.class})
@Documented
public @interface EnumValue {

    String message() default "{box.validator.constraints.enum}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 需要校验值的枚举
     */
    Class<? extends java.lang.Enum<? extends IBaseEnum<?>>> value();

    /**
     * 只比较本身
     * <p>类似集合，数组，是比较里面的值，还是集合本身，因为有可能枚举的value就是集合。因此需要设置为true</p>
     * <p>默认会处理集合或数组里面的值</p>
     * 默认false
     */
    boolean equalsRoot() default false;
}

```
value 只能传递继承了`IBaseEnum`的枚举，校验字段值只能为(`com.zhituanbox.core.enums.IBaseEnum#getValue`)中的值

默认对集合与数组类型，会校验迭代内容，不会校验值本身。
如果`getValue`刚好是集合或者数组类型，则希望校验对象本身，则需要将`equalsRoot`设置为`true`

> `equalsRoot` 此属性只在校验数据类型为`集合`或者`数组`类型时有效

#### 1.6.1.1 示例
> 以上面`NumberEnum`为例

```java
private static class UserTest {

        @EnumValue(NumberEnum.class)
        private Integer id;

        @EnumValue(NumberEnum.class)
        private Set<Integer> idSet;

        @EnumValue(NumberEnum.class)
        private Integer[] idArray;

        public UserTest(Integer id, Set<Integer> idSet, Integer[] idArray) {
            this.id = id;
            this.idSet = idSet;
            this.idArray = idArray;
        }
    }
```

`id`,`idSet`,`idArray`值不为`null`时，只能为[0,1,2]

### 1.6.2 `@ID`数据库主键校验
```java
public @interface ID {

    String message() default "{box.validator.constraints.id}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 小写字母
     * 默认可包含小写字母
     * <p><b>只有String类型才生效</b></p>
     */
    boolean lowerCase() default true;

    /**
     * 大写字母
     * 默认可包含大写字母
     * <p><b>只有String类型才生效</b></p>
     */
    boolean upperCase() default true;
}
```

支持`Long`,`Integer`,`String`类型值的校验。
常用主键也都是`正整数`或者`UUID(去横杠)`

- 如果是数值类型;值必须大于0
- 如果是字符串类型;值只能包含数字与大小写字母

> 如果需要限定字符串不能包含大写字母或者小写字母，设置属性`upperCase`或者`lowerCase`为`false`即可

### 1.6.3.`@Name`常用名称校验
```java
public @interface Name {

    String message() default "{box.validator.constraints.name}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 允许开头有空格
     * 默认：false（不允许）
     */
    boolean startBlank() default false;

    /**
     * 允许结尾有空格
     * 默认：false（不允许）
     */
    boolean endBlank() default false;

    /**
     * 允许包含空格
     * 默认：false（不允许）
     */
    boolean containsBlank() default false;
}
```

通常的名称不能包含空格，如果需要包含空格，可设置`startBlank`,`endBlank`,`containsBlank`属性

> 不为null并且长度大于0，才会生效校验；可以搭配@NotBlank一起使用



### 1.6.4.`@NotContainsNull` 数组集合不能包含`null`值

> 支持数组类型和`java.util.Collection`集合类型

#### 1.6.4.1示例

```java
private static class UserTest {

        @NotContainsNull
        private Set<Integer> idSet;

        @NotContainsNull
        private Integer[] idArray;
    }

```
`idSet`和`idArray`中的值，都不能包含`null`


## 1.7.支持会话传递的数据库线程池`SessionExecutors`

[src/main/java/com/zhituanbox/core/thread/README.md](src/main/java/com/zhituanbox/core/thread/README.md)
